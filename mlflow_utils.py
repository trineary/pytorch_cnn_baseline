import os
import git
import datetime
import mlflow.pytorch
from matplotlib import pyplot as plt


class MLFlowUtils(object):
    def __init__(self):
        self.repo = None
        self.init_repo_ref(project_path=None)
        # Initialize environment which gathers dependencies, etc.
        mlflow.pytorch.get_default_conda_env()
        pass

    def init_repo_ref(self, project_path=None):
        if project_path is None:
            project_path = os.getcwd()
        try:
            self.repo = git.Repo(project_path)
        except Exception as e:
            print("Exception while initializing repo: ", e)
            print("Project path: ", project_path)
        pass

    def init_db_tracking_uri(self, db_str="sqlite:///mlruns.db"):
        # mlflow.set_tracking_uri(uri_path)
        mlflow.set_tracking_uri(db_str)
        pass

    def get_git_hash(self, hash_bits=8, project_path=None):
        if self.repo is None:
            print("Git repo path hasn't been initialized.  Using default local path.")
            self.init_repo_ref(project_path)
        commit_hash = self.repo.head.object.hexsha[:hash_bits]
        return commit_hash

    def git_commit(self, commit_str="Commit string"):
        # Get a list of files that have changed.  If > 0 then add and commit.
        # This will generate a new git hash.
        files = self.repo.git.diff(None, name_only=True)
        if len(files) > 0:
            for f in files.split('\n'):
                self.repo.git.add(f)
            self.repo.git.commit('-m', commit_str)
        pass

    def save_model_path_proj_ts(self, mlflow_run, base_save_path="/home/pneary/models"):
        # Create save path
        ts = datetime.datetime.now()
        _date = ts.strftime("%Y_%m_%d")
        save_path = "%s/%s" % (base_save_path, _date)
        None if os.path.exists(save_path) else os.makedirs(save_path)

        # Get the git hash and download the specified hash from the specified run to the save path
        run_hash = self.get_git_hash(hash_bits=8)
        client = mlflow.tracking.MlflowClient()
        saved_path = client.download_artifacts(mlflow_run.info.run_id, run_hash, save_path)
        print("Saved new model to: ", saved_path)
        return saved_path

    def save_model_to_path(self, mlflow_run, base_save_path="/home/pneary/models"):
        # Create save path
        save_path = "%s" % (base_save_path)
        None if os.path.exists(save_path) else os.makedirs(save_path)

        # Get the git hash and download the specified hash from the specified run to the save path
        run_hash = self.get_git_hash(hash_bits=8)
        client = mlflow.tracking.MlflowClient()
        saved_path = client.download_artifacts(mlflow_run.info.run_id, run_hash, save_path)
        print("Saved new model to: ", saved_path)
        return saved_path

    def save_plt_artifact(self, lines_list, line_labels, save_name="training_plots.png", base_save_path=None):
        # Generate plot, save it, then load saved image to mlflow.
        x = range(1, len(lines_list[0]) + 1)

        # Clear plots of anything that may have been previously written
        plt.clf()
        plt.cla()
        # Add all lines in list with labels to the plot
        for plot_line, line_label in zip(lines_list, line_labels):
            plt.plot(x, plot_line, label=line_label)
        # Add info to the plot title, x/y labels, and enable legend
        plt.title('Training Info')
        plt.xlabel('x-axis')
        plt.ylabel('y-axis')
        plt.legend()

        # Save plot to path on disk
        if base_save_path is None:
            save_root_path = "./images"
            None if os.path.exists(save_root_path) else os.makedirs(save_root_path)
        else:
            save_root_path = os.path.normpath(base_save_path + os.sep + "images")
            None if os.path.exists(save_root_path) else os.makedirs(save_root_path)
        save_path = os.path.normpath(save_root_path + os.sep + save_name)
        plt.savefig(save_path)

        # Add plot to mlflow from saved path
        mlflow.log_artifact(save_path)
        return save_path

    def run_model_train(self, train_fcn, pytorch_model, save_base_path=None):
        # Example of how to piece everything together

        # Commit any code changes
        self.git_commit(commit_str="Auto commit in mlflow_utils.runtrain.py")

        # run_id - run id.  If specified, load parameters from this id
        # experiment_id - id to lot this experiment under (don't set if run_id is specified)
        # run_name - only used if run_id not specified
        run_hash = self.get_git_hash(hash_bits=8)
        mlflow.tracking.MlflowClient(tracking_uri="sqlite:///mlruns.db", registry_uri=None)
        with mlflow.start_run() as mlflow_run:
            train_fcn()
            mlflow.pytorch.log_model(pytorch_model, artifact_path=run_hash)  # model_repo_path

        # Move the artifact folder to another directory to keep project code clean
        if save_base_path is not None:
            self.save_model_path_proj_ts(mlflow_run, base_save_path=save_base_path)

        print("Finished running main_train_mlflow.py")
        pass

    def load_model_from_path(self, saved_model_path):
        loaded_model = mlflow.pytorch.load_model(saved_model_path)
        return loaded_model


