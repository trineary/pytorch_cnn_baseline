import argparse
import project_templates


parser = argparse.ArgumentParser(description="aws_sample")

# Specify which project template we want to use here.
parser.add_argument('--template', default="aws", help="Selected template for modifying options")

# Number of training epochs
parser.add_argument('--epochs', type=int, default=5, help="Training epochs")

# Learning rate for optimizer
parser.add_argument('--lr', type=float, default=1e-3, help="Learning rate")

# Batch size for training
parser.add_argument('--batch_size', type=int, default=32, help="Batch size")

# How to split up data set for training, validation, and testing
parser.add_argument('--train_percent', type=float, default=0.7, help="Data set training split")
parser.add_argument('--val_percent', type=float, default=0.15, help="Data set validation split")
parser.add_argument('--test_percent', type=float, default=0.15, help="Data set testing split")

# Select which optimizer to use.
# "sgd", "adamax", "adam", "eve", "nadam", "adadelta", "adagrad", "rmsprop"
parser.add_argument('--optimizer', type=str, default="adam", help="Selected optimizer")

# Specify momentum to use in optimizer.  TODO: consider how to handle additional optimizer settings
parser.add_argument('--momentum', type=float, default=0.9, help="Optimizer momentum")

# Specify early stop to help guard against over training
parser.add_argument('--early_stop_val', type=float, default=0.99, help="Set value for early training stop")

# Path for saving training products
parser.add_argument('--save_path', type=str, default="./", help="Base path for saving training outputs and products")

# Specify whether to use transfer learning or not. TODO: store_true/store_false don't appear to be working as expected
#                                                   need to investigate.  For now set explicitly in project_templates.py
parser.add_argument('--tx_learning', action="store_false", help="Specify whether to use transfer learning")

# Specify whether to use pretrained weights.  Set explicitly in project_templates.py
parser.add_argument('--pretrained_weights', action="store_false", help="Specify whether to use pre-trained weights")

# Specify whether to use GPU or not.  Set explicitly in project_templates.py
parser.add_argument('--use_gpu', action="store_true", help="Specify whether to use GPU or not")

# Select architecture to use.  Other options found in /cnna_rchs/sotacnn.py  TODO: Add handling for custom models
parser.add_argument('--model', type=str, default="resnet50", help="CNN model to work with ")

# Set depth of images coming into CNN
parser.add_argument('--image_depth', type=int, default=3, help="Image depth, 3 for RGB, 1 for grayscale")

# Specify whether to save trained model
parser.add_argument('--save_trained_model', action="store_true", help="Specify whether to save trained models")

# Specify whether to augment training or not
parser.add_argument('--augment', action="store_true", help="Specify whether to augment training")

# Set number of workers to use for loading images.  Careful, workers respawned with every epoch.
parser.add_argument('--workers', type=int, default=1, help="Number of workers for loading images")

# Specify data source directory
parser.add_argument('--data_source_dir', type=str, default="E:\\Projects\\aws_work_sample\\geological_similarity",
                    help="Path to image root directory")

# Specify output path for training products
parser.add_argument('--output_root_dir', type=str, default="E:\\Project_output\\aws_work_sample\\geological_similarity",
                    help="Path to training output")

# Name of directory that models are saved to
parser.add_argument('--model_dir', type=str, default="saved_models",
                    help="Path to training output")

# Name of directory for artifacts
parser.add_argument('--artifact_dir', type=str, default="artifacts",
                    help="Path to training output")

# MLFlow experiment identifier - this groups experiments together in the UI
parser.add_argument('--mlflow_experiment_id', type=str, default="default_id",
                    help="MLFlow experiment identifier")

args = parser.parse_args()

# Update args with project specific settings
project_templates.set_template(args)


