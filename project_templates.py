
def set_template(args):

    if args.template == "aws":
        args.model = "resnet50"  # other options: resnet50, resnext50_32x4d, wide_resnet50_2
        args.gpus = [0]
        args.use_gpu = True
        args.augment = True
        args.save_trained_model = True
        args.image_depth = 3
        args.workers = 1
        args.epochs = 5
        args.mlflow_experiment_id = "cnn_compare"
        args.optimizer = "adam"  # "sgd", "adamax", "adam", "nadam", "adadelta", "adagrad", "rmsprop"
        args.data_source_dir = "E:\\Projects\\aws_work_sample\\geological_similarity"
        args.output_root_dir = "E:\\Project_output\\aws_work_sample\\geological_similarity"


