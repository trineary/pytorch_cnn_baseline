
import torch.nn as nn
import torchvision.models as models


supported_models = ["alexnet", "densenet121", "inception_v3", "vgg19", "squeezenet1_0", "shufflenet_v2_x0_5",
                    "mobilenet_v2", "resnet50", "resnext50_32x4d", "wide_resnet50_2"]


def get_model_mean_std(model_name=None):
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    return mean, std


def load_model(model_name, num_classes, pretrained, tx_learning, img_depth=None, reqd_input_size=None):
    model = None

    # To get inception to work I'll need to follow the instructions here: https://pytorch.org/hub/pytorch_vision_inception_v3/
    # It has a requirement for normalizing data and it has a minimum image height/width requirement.  I don't meet either of these currently

    # From https://pytorch.org/tutorials/beginner/finetuning_torchvision_models_tutorial.html
    def freeze_weights(_model, _tx_learning):
        # if tx_learning is true, then weights in the main architecture are frozen and only the output layer is changed
        if _tx_learning is True:
            for _param in _model.parameters():
                _param.requires_grad = False

    if model_name == "alexnet":
        model = models.alexnet(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        if img_depth is not None:
            model.features[0] = nn.Conv2d(img_depth, 64, kernel_size=(11, 11), stride=(2, 2), padding=(2, 2))
            # model.features[0] = nn.Conv2d(img_depth, 96, kernel_size=(11, 5), stride=(2, 2), padding=(2, 2))
            model.features[2] = nn.MaxPool2d(2, stride=2)
        num_ftrs = model.classifier[6].in_features
        model.classifier[6] = nn.Linear(num_ftrs, num_classes, bias=True)
        reqd_input_size = None
    elif model_name == "densenet121":
        model = models.densenet121(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        if img_depth is not None:
            model.features[0] = nn.Conv2d(img_depth, 64, kernel_size=(3, 3), stride=(1, 1), bias=False)
        num_ftrs = model.classifier.in_features
        model.classifier = nn.Linear(num_ftrs, num_classes)
        reqd_input_size = None
    elif model_name == "inception_v3":
        model = models.inception_v3(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        # if img_depth is not None:
        #     model.features[0] = nn.Conv2d(img_depth, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        # Handle the auxilary net
        num_ftrs = model.AuxLogits.fc.in_features
        model.AuxLogits.fc = nn.Linear(num_ftrs, num_classes)
        # Handle the primary net
        num_ftrs = model.fc.in_features
        model.fc = nn.Linear(num_ftrs, num_classes)
        # reqd_input_size = 299
        reqd_input_size = None
    elif model_name == "vgg19":
        model = models.vgg19(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        if img_depth is not None:
            model.features[0] = nn.Conv2d(img_depth, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        num_ftrs = model.classifier[6].in_features
        model.classifier[6] = nn.Linear(num_ftrs, num_classes)
    elif model_name == "squeezenet1_0":
        model = models.squeezenet1_0(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        if img_depth is not None:
            model.features[0] = nn.Conv2d(img_depth, 96, kernel_size=(7, 7), stride=(2, 2))
        model.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1, 1), stride=(1, 1))
        reqd_input_size = None
    elif model_name == "squeezenet1_1":
        model = models.squeezenet1_1(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        if img_depth is not None:
            model.features[0] = nn.Conv2d(img_depth, 64, kernel_size=(3, 3), stride=(2, 2))
        model.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1, 1), stride=(1, 1))
        reqd_input_size = None
    elif model_name == "shufflenet_v2_x0_5":
        model = models.shufflenet_v2_x0_5(pretrained=pretrained)
        freeze_weights(model, num_classes)
        # if img_depth is not None:
        #     # model.conv1[0] = nn.Conv2d(img_depth, 24, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False)
        #     model.conv1[0] = nn.Conv2d(img_depth, 24, kernel_size=(11, 11), stride=(3, 3), padding=(1, 1), bias=False)
        num_ftrs = model.fc.in_features
        model.fc = nn.Linear(num_ftrs, num_classes)
        # reqd_input_size = None
        reqd_input_size = 224
    elif model_name == "mobilenet_v2":
        model = models.mobilenet_v2(pretrained=pretrained)
        freeze_weights(model, tx_learning)
        if img_depth is not None:
            model.features[0][0] = nn.Conv2d(img_depth, 32, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False)
        num_ftrs = model.classifier[1].in_features
        model.classifier[1] = nn.Linear(num_ftrs, num_classes)
        reqd_input_size = None
        # reqd_input_size = 224
    elif model_name == "resnet50":
        model = models.resnet50(pretrained=pretrained)
        freeze_weights(model, num_classes)
        # if img_depth is not None:
        #     model.conv1 = nn.Conv2d(img_depth, 64, kernel_size=(3, 3), stride=(2, 2), bias=False)
        num_ftrs = model.fc.in_features
        model.fc = nn.Linear(num_ftrs, num_classes, bias=True)
        reqd_input_size = None
    elif model_name == "resnext50_32x4d":
        model = models.resnext50_32x4d(pretrained=pretrained)
        freeze_weights(model, num_classes)
        # if img_depth is not None:
        #     model.conv1 = nn.Conv2d(img_depth, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        num_ftrs = model.fc.in_features
        model.fc = nn.Linear(num_ftrs, num_classes, bias=True)
        reqd_input_size = None
    elif model_name == "wide_resnet50_2":
        model = models.wide_resnet50_2(pretrained=pretrained)
        freeze_weights(model, num_classes)
        # if img_depth is not None:
        #     model.conv1 = nn.Conv2d(img_depth, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        num_ftrs = model.fc.in_features
        model.fc = nn.Linear(num_ftrs, num_classes, bias=True)
        reqd_input_size = None
    else:
        print("Model name not in supported list: ", model_name)
        exit()

    num_filters = 16
    model = nn.Sequential(
                          #nn.Conv2d(img_depth, img_depth, kernel_size=(11, 11), stride=(7, 7)),
                          nn.Conv2d(img_depth, 3, kernel_size=(3, 3), stride=(2, 2)),
                          nn.ReLU(),
                          # nn.Conv2d(img_depth, num_filters, kernel_size=(3, 3), stride=(2, 2)),
                          # nn.ReLU(),
                          # nn.Conv2d(num_filters, 3, kernel_size=(3, 3), stride=(2, 2)),
                          # nn.ReLU(),
                          model
                          )

    model.train()
    # print(model)
    return model, reqd_input_size


def set_model_lrs(model_name, model, base_lr, layer_lr, optimizer, img_depth=None):
    multi_lr_opt = None
    layer_lr_list = []

    if model_name == "alexnet":
        if img_depth is not None:
            layer_lr_list = ['features.0.weight', 'features.0.bias', 'classifier.6.weight', 'classifier.6.bias']
        else:
            layer_lr_list = ['classifier.6.weight', 'classifier.6.bias']
    elif model_name == "resnet50":
        if img_depth is not None:
            layer_lr_list = ['conv1.weight', 'conv1.bias', 'fc.weight', 'fc.bias']
        else:
            layer_lr_list = ['fc.weight', 'fc.bias']
    else:
        return multi_lr_opt

    params = list(map(lambda x: x[1], list(filter(lambda kv: kv[0] in layer_lr_list, model.named_parameters()))))
    base_params = list(map(lambda x: x[1], list(filter(lambda kv: kv[0] not in layer_lr_list, model.named_parameters()))))
    multi_lr_opt = optimizer([{'params': base_params}, {'params': params, 'lr': layer_lr}], lr=base_lr)

    return multi_lr_opt


def view_arch_info(model_name, update_dims=False):
    pretrained = False

    if update_dims is False:
        if model_name == "alexnet":
            model = models.alexnet(pretrained=pretrained)
        elif model_name == "densenet121":
            model = models.densenet121(pretrained=pretrained)
        elif model_name == "inception_v3":
            model = models.inception_v3(pretrained=pretrained)
        elif model_name == "vgg19":
            model = models.vgg19(pretrained=pretrained)
        elif model_name == "squeezenet1_0":
            model = models.squeezenet1_0(pretrained=pretrained)
        elif model_name == "squeezenet1_1":
            model = models.squeezenet1_1(pretrained=pretrained)
        elif model_name == "shufflenet_v2_x0_5":
            model = models.shufflenet_v2_x0_5(pretrained=pretrained)
        elif model_name == "mobilenet_v2":
            model = models.mobilenet_v2(pretrained=pretrained)
        elif model_name == "resnet50":
            model = models.resnet50(pretrained=pretrained)
        elif model_name == "resnext50_32x4d":
            model = models.resnext50_32x4d(pretrained=pretrained)
        elif model_name == "wide_resnet50_2":
            model = models.wide_resnet50_2(pretrained=pretrained)
    else:
        model, _ = load_model(model_name, num_classes=32, pretrained=None, tx_learning=False)

    print("Loaded model: ", model_name)
    print(model)
    pass


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def print_arch_param_stats(num_classes, tx_learning=False):
    print("-" * 50)
    print("Number of trainable parameters in architecture when transfer learning is: ", tx_learning, " and %d classes"
          % num_classes)
    print("-" * 50)
    print("Architecture Name / Number of parameters")
    for model_name in ["shufflenet_v2_x0_5", "mobilenet_v2", "alexnet", "resnet50", "squeezenet1_0", "resnext50_32x4d",
                       "wide_resnet50_2", "densenet121"]:
        model, _ = load_model(model_name, num_classes, pretrained=None, tx_learning=tx_learning, img_depth=3)
        num_params = count_parameters(model)
        print("%s,\t %d" % (model_name, num_params))
    print("-" * 50)
    pass


if __name__ == "__main__":
    # model_list = ["alexnet", "densenet121", "inception_v3", "vgg19", "squeezenet1_0", "shufflenet_v2_x0_5",
    #               "mobilenet_v2", "resnet50", "resnext50_32x4d", "wide_resnet50_2"]
    # Code to look at architecture info
    # print("Running view_arch_info...")
    view_arch_info("wide_resnet50_2")

    # print_arch_param_stats(num_classes=32, tx_learning=False)
    # print_arch_param_stats(num_classes=16, tx_learning=False)
    # print_arch_param_stats(num_classes=32, tx_learning=True)
    # print_arch_param_stats(num_classes=16, tx_learning=True)
