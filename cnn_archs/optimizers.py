
import torch.optim as optim
from pywick import optimizers


def load_optimizer(model, opt_select, lr, opt_momentum=None):
    if opt_select == "adam":
        optimizer = optim.Adam(model.parameters(), lr=lr)
    elif opt_select == "sgd":
        if opt_momentum is None:
            print("Error! Please initialize opt_momentum")
            return None
        optimizer = optim.SGD(model.parameters(), lr=lr, momentum=opt_momentum)
    elif opt_select == "adamax":
        optimizer = optim.Adamax(model.parameters(), lr=lr)
    elif opt_select == "eve":
        optimizer = optimizers.eve.Eve(model.parameters(), lr=lr)
    elif opt_select == "nadam":
        optimizer = optimizers.nadam.Nadam(model.parameters(), lr=lr)
    elif opt_select == "adadelta":
        optimizer = optim.Adadelta(model.parameters(), lr=lr)
    elif opt_select == "adagrad":
        optimizer = optim.Adagrad(model.parameters(), lr=lr)
    elif opt_select == "asgd":
        optimizer = optim.ASGD(model.parameters(), lr=lr)
    elif opt_select == "rmsprop":
        if opt_momentum is None:
            print("Error! Please initialize opt_momentum")
            return None
        optimizer = optim.RMSprop(model.parameters(), lr=lr, momentum=opt_momentum)

    else:
        return None

    print("--> Loading and returning optimizer: ", opt_select)
    return optimizer
