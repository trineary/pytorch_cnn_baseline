
import glob
import os
from PIL import Image


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def get_first_image_width(root_dir, image_types=["*.jpg", "*.png", "*.tif", "*.tiff"]):
    #  Given a root directory, return width of image
    for image_type in image_types:
        subdirs = os.listdir(root_dir)
        img_path = os.path.normpath(root_dir + os.sep + subdirs[0]) + os.sep
        for img_file in glob.glob(img_path + image_type):
            if len(img_file) > 0:
                im = Image.open(img_file)
                return im.size[0]
            break
    return None





