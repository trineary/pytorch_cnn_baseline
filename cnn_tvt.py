
from __future__ import print_function

import time
from torchtools.dir_loader import get_loaders_from_rootdirs

from torchtools.tvt_utils import *
from cnn_archs.optimizers import load_optimizer
from cnn_archs.sotacnn import *
import mlflow.pytorch
from mlflow_utils import MLFlowUtils
import datetime


class TVT(object):
    def __init__(self, args):

        # Set args, identify number of classes, class names
        self.args = args
        self.num_image_classes = self.get_num_classes(args.data_source_dir)
        self.class_names = self.get_class_names(args.data_source_dir)

        # Print some info to the terminal
        self.print_starting_info()
        # Initialize save paths for products
        self.init_save_paths()
        pass

    def get_num_classes(self, root_dir):
        #  Given a root directory, return number of folders (i.e. classes)
        subdirs = os.listdir(root_dir)
        return len(subdirs)

    def get_class_names(self, root_dir):
        #  Given a root directory, return number of folders (i.e. classes)
        subdirs = os.listdir(root_dir)
        return subdirs

    def print_starting_info(self):
        print("-" * 50)
        print("Starting training: ")
        print("\tModel: ", self.args.model)
        print("\tEpochs: ", self.args.epochs)
        print("\tLR: ", self.args.lr)
        print("\tBatch size: ", self.args.batch_size)
        print("\tOptimizer: ", self.args.optimizer)
        print("\tImage source root: ", self.args.data_source_dir)
        print("\tClass names: ", self.class_names)
        print("\tNum classes: ", self.num_image_classes)
        print("-" * 50)
        pass

    def init_save_paths(self):
        # Initialize paths for results
        self.trained_model_path = self.args.output_root_dir + os.sep + self.args.model_dir

        # If paths not present then create
        self.init_path(self.args.output_root_dir)
        self.init_path(self.trained_model_path)
        pass

    def init_path(self, dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        pass

    def init_training_model(self):
        # Specify percentages for splitting training set
        tvt_percents = [self.args.train_percent, self.args.val_percent, self.args.test_percent]

        # Load model
        self.model, self.reqd_input_size = load_model(model_name=self.args.model, num_classes=self.num_image_classes,
                                                      pretrained=self.args.pretrained_weights,
                                                      tx_learning=self.args.tx_learning, img_depth=self.args.image_depth)

        # Put model on CPU/GPU
        self.model, self.device, pin_memory = init_model_device(self.model, gpus_selected=self.args.gpus)

        # Initialize the optimizer
        self.optimizer = load_optimizer(model=self.model, opt_select=self.args.optimizer, lr=self.args.lr,
                                        opt_momentum=self.args.momentum)

        # Chance to load mean/std, used for normalization in augmentation phase during training
        mean, std = get_model_mean_std(self.args.model)

        # Initialize each of the data loaders
        [self.train_loader, self.val_loader, self.test_loader] = get_loaders_from_rootdirs([self.args.data_source_dir],
                                                                 batch_size=self.args.batch_size, augment=self.args.augment,
                                                                 train_percent=self.args.train_percent,
                                                                 valid_percent=self.args.val_percent,
                                                                 test_percent=self.args.test_percent,
                                                                 num_workers=self.args.workers, pin_memory=pin_memory,
                                                                 mean=mean, std=std,
                                                                 load_grayscale=True if self.args.image_depth == 1 else False)
        pass

    def train_cnn(self, lr_scheduler=None):

        # Get MLFlow ready to go
        flow = MLFlowUtils()
        flow.git_commit(commit_str="Training run from mlflow_example.py")
        run_hash = flow.get_git_hash(hash_bits=8)
        # Initialize mlflow
        mlflow.set_tracking_uri("")
        mlflow.set_experiment(self.args.mlflow_experiment_id)

        # Load/initialize model for training
        self.init_training_model()

        best_val_loss = 100000
        train_acc_history, train_loss_history = [], []
        val_acc_history, val_loss_history = [], []

        print("Start training")
        start_time = time.time()
        # Train with mlflow running so information of interest can be captured
        with mlflow.start_run(run_name=self.args.mlflow_experiment_id) as mlflow_run:
            # Train for N epochs
            for epoch_count in range(self.args.epochs):
                print("Epoch: %d" % epoch_count)

                train_acc, train_loss = train_epoch(self.model, self.optimizer, self.train_loader, device=self.device,
                                                  scheduler=lr_scheduler)
                train_acc_history.append(train_acc)
                train_loss_history.append(train_loss)

                # Add loss/accuracy to mlflow
                mlflow.log_metrics({"train_loss": train_loss, "train_accuracy": train_acc}, step=epoch_count)

                # Gather validation accuracy/loss
                val_acc, val_loss = test(self.model, self.val_loader, self.device, label_names=self.class_names)
                val_acc_history.append(val_acc)
                val_loss_history.append(val_loss)

                # Log validation accuracy/loss information
                mlflow.log_metrics({"val_loss": val_loss, "val_accuracy": val_acc}, step=epoch_count)

                # Print statement to visually observe progress
                print("--> train epoch %d,  arch: %s, opt: %s, lr %0.5f, batch size: %d, train_acc: %0.3f, train_loss: %0.3f, val_acc: %0.3f, val_loss: %0.3f, in size: " %
                      (epoch_count, self.args.model, self.args.optimizer, self.args.lr, self.args.batch_size,
                       train_acc, train_loss, val_acc, val_loss), self.reqd_input_size,
                      ", total time: ", time.time() - start_time)

                # Determine if we've achieved an improved model and want to take a snapshot
                if self.args.save_trained_model is True:
                    if val_loss < best_val_loss:
                        mlflow.pytorch.log_model(self.model, artifact_path=run_hash)  # model_repo_path
                        self.model_saved_path = flow.save_model_to_path(mlflow_run, base_save_path=self.trained_model_path)
                        best_val_loss = val_loss

                # Determine if we're going to stop early
                if val_acc >= self.args.early_stop_val and epoch_count >= 2:
                    break

            # Log metrics to mlflow
            mlflow.log_metric("epochs", self.args.epochs)
            mlflow.log_param("lr", self.args.lr)
            mlflow.log_param("model", self.args.model)
            mlflow.log_param("optimizer", self.args.optimizer)
            flow.save_plt_artifact(lines_list=[train_acc_history, val_acc_history], line_labels=["Train-acc", "Val-acc"],
                                   save_name="accuracy_plots.png", base_save_path=self.args.output_root_dir)
            flow.save_plt_artifact(lines_list=[train_loss_history, val_loss_history], line_labels=["Train-loss", "Val-loss"],
                                   save_name="loss_plots.png", base_save_path=self.args.output_root_dir)

        return self.model_saved_path

    def test_cnn(self, saved_model_path=None, model_hash=None):

        # Open model by either saved path or model hash/directory (which can be found in mlflow ui)
        if saved_model_path is not None:
            saved_model_path = saved_model_path
        elif model_hash is not None:
            saved_model_path = os.path.normpath(self.trained_model_path + os.sep + model_hash)
        elif saved_model_path is None:
            saved_model_path = self.model_saved_path

        # Load the saved model with mlflow
        saved_model = mlflow.pytorch.load_model(saved_model_path)

        # Run data from the test loader through the trained model and return accuracy/loss
        test_acc, test_loss = test(saved_model, self.test_loader, self.device, show_cm=True,
                                   label_names=self.class_names)

        # TODO: Add Test accuracy/loss to mlflow
        # TODO: Add confusion matrix to mlflow

        return test_acc, test_loss


