# --------------------------------------------------------------------------------------------------------------------
#
# Patrick Neary
# Date: 3/13/19
#
# cnnstats.py
#
# Class for working with stats.  Includes confusion matrix, bit error rate, and general accuracy (so far).
#
# --------------------------------------------------------------------------------------------------------------------

import matplotlib.pyplot as plt
from decimal import Decimal
from sklearn.metrics import confusion_matrix
import seaborn as sns
import numpy as np


class CNNStats(object):
    def __init__(self, class_names=None, use_oam_cols=True):
        self._truth = []
        self._pred = []
        self._class_names = class_names
        self._use_oam_cols = use_oam_cols
        self._correct_count = 0
        self._total_count = 0
        self._truth_list = []
        self._pred_list = []
        pass

    def set_class_names(self, class_names):
        self._class_names = class_names

    def reset(self):
        self._truth = []
        self._pred = []
        self._correct_count = 0
        self._total_count = 0

    def add_truth_pred(self, truth, pred):
        self._truth += list(truth)
        self._pred += list(pred)
        for p, t in zip(pred, truth):
            if p == t:
                self._correct_count += 1
        self._total_count += len(truth)
        pass

    def get_confusion_matrix(self):
        # print("-"*50)
        # print("get confusion matrix")
        # print("types: ", type(self._truth), type(self._pred))
        # print("types: ", type(self._truth[0]), type(self._pred[0]))
        # print(self._truth)
        # print(self._pred)
        # print(self._class_names)
        cm = confusion_matrix(self._truth, self._pred)
        return cm

    def print_confusion_matrix(self):
        np.set_printoptions(linewidth=300)
        cm = self.get_confusion_matrix()
        print("\nConfusion Matrix:")
        print(self._class_names)
        print(cm)
        print("\n")

        accuracy = self._correct_count / self._total_count
        print("Total images: ", self._total_count)
        print("Correct images: ", self._correct_count)
        print("Accuracy: %.2f" % (100 * accuracy))
        pass

    def cm_plot(self):
        num_elements = len(self._class_names)
        c_mat = np.zeros((num_elements, num_elements))
        c_mat_z = self.get_confusion_matrix()
        for idx, mat_row in enumerate(c_mat_z):
            row_sum = np.sum(mat_row)
            c_mat[idx] = mat_row / row_sum

        plt.rcParams['figure.figsize'] = [10, 10]
        plt.clf()
        ax = sns.heatmap(c_mat, annot=True, cbar=False, fmt="0.3f")
        bottom, top = ax.get_ylim()
        ax.set_xlim(top - 0.5, bottom + 0.5)
        ax.set_ylim(top - 0.5, bottom + 0.5)
        tick_marks = np.arange(len(self._class_names)) + 0.5
        plt.xticks(tick_marks, self._class_names, rotation=45)
        plt.yticks(tick_marks, self._class_names, rotation=0)
        # plot_name =
        plt.show()
        pass

    def print_confusion_matrix_latex(self, row_labels=None):
        if self._use_oam_cols is True:
            self._class_names = self.get_index_col_hdrs()

        np.set_printoptions(linewidth=300)
        cm = self.get_confusion_matrix()
        print("\nConfusion Matrix:")
        print(self._class_names)
        print()
        print("\\begin{table}[htbp]")
        print("%\\begin{table*}[htbp]")
        print("\\centering")
        print("\\caption{\\bf %s}" % "Table caption")
        if row_labels is not None:
            columns = "|" + "c|" * (len(cm[0]) + 1)
            columns2 = "c" * (len(cm[0]) + 1)
        else:
            columns = "|" + "c|" * (len(cm[0]))
            columns2 = "c" * (len(cm[0]))
        print("%\\resizebox{\\textwidth}{!}{")
        print("\\begin{tabular}{", columns, "}")
        print("%\\begin{tabular}{", columns2, "}")
        print("\\hline")

        if self._class_names is not None:
            if len(self._class_names) > 0:
                col_headers = ""
                for idx, col_header in enumerate(self._class_names):
                    if idx == 0:
                        col_headers += "" + col_header
                    else:
                        col_headers += " & " + col_header
                print(col_headers, "\\"*2)
                print("\\hline")

        for row in cm:
            row_data = ""
            for idx, col_entry in enumerate(row):
                if row_labels is not None and idx == 0:
                    row_data += row_labels[idx]
                elif idx == 0:
                    # row_data += " %.3f" % col_entry
                    row_data += " %d" % col_entry
                else:
                    # row_data += " & %.3f" % col_entry
                    row_data += " & %d" % col_entry
                # sci_notation = "{:.2e}".format(col_entry)
                # row_data += " & %s" % sci_notation
            print(row_data, "\\" * 2)
        print("\\hline")
        print("\\end{tabular}")
        print("%}")
        print("\\label{tab:table_name}")
        print("\\end{table}")
        print("%\\end{table*}")
        print()
        print("\n")

        accuracy = self._correct_count / self._total_count
        print("Total images: ", self._total_count)
        print("Correct images: ", self._correct_count)
        print("Accuracy: %.2f" % (100 * accuracy))
        pass

    def print_ber(self):
        ber = (self._total_count - self._correct_count) / self._total_count
        # conf_level = 1 - math.exp(-self._total_count * ber)

        print("Bit Error Rate: %.2E" % Decimal(ber))
        # print("Confidence Level: %.2f" % (conf_level * 100))
        pass

    def get_oam_col_hdrs(self, num_classes=None):
        if num_classes is None:
            num_classes = np.max(np.asarray(self._truth)) + 1
            print("number of classes: ", num_classes, type(num_classes))
        oam_col_titles = []
        for i in range(num_classes):
            if num_classes > 16:
                col_title = "0x{0:05b}".format(i)
            else:
                col_title = "0x{0:04b}".format(i)
            oam_col_titles.append(col_title)
        return oam_col_titles

    def get_index_col_hdrs(self, num_classes=None):
        if num_classes is None:
            num_classes = np.max(np.asarray(self._truth)) + 1
            print("number of classes: ", num_classes, type(num_classes))
        oam_col_titles = []
        for i in range(num_classes):
            col_title = "%d" % i
            oam_col_titles.append(col_title)
        return oam_col_titles

def main():
    cm = CNNStats()
    cm.add_truth_pred([0, 0, 0, 0, 1, 1, 2, 2, 2, 2], [2, 0, 0, 1, 1, 1, 2, 1, 2, 2])
    cm.add_truth_pred([0, 2], [2, 2])
    row_labels = ["row1", "row2", "row3"]
    # row_labels = None
    cm.print_confusion_matrix_latex(row_labels)
    oam_col_titles = cm.get_oam_col_hdrs()
    print(oam_col_titles)
    pass


if __name__ == '__main__':
    main()
