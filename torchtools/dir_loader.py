
import torch
import numpy as np

from torchvision import datasets
from torchvision import transforms
from torch.utils.data.sampler import SubsetRandomSampler


def get_loaders_from_rootdirs(root_dirs, batch_size, augment, train_percent=0.7, valid_percent=0.1,  test_percent=0.2,
                              num_workers=4, pin_memory=False, mean=None, std=None, load_grayscale=False):
    transform_list = []

    # Add transforms if configured to do so
    transform_list.append(transforms.ToTensor())
    if mean is not None and std is not None: transform_list.append(transforms.Normalize(mean, std))
    if load_grayscale is True: transform_list.append(transforms.Grayscale())
    if augment is True: transform_list.append(transforms.RandomHorizontalFlip())
    if augment is True: transform_list.append(transforms.RandomVerticalFlip())

    gen_transform = transforms.Compose(transform_list)

    dataset_list = []
    # Load data from list of root directories
    for img_root_dir in root_dirs:
        # This loads images in order, so all of the first folder are loaded first, etc.
        folder_dataset = datasets.ImageFolder(
            root=img_root_dir,
            transform=gen_transform,
        )
        dataset_list.append(folder_dataset)

    # Concatenate lists together
    concat_list = torch.utils.data.ConcatDataset(dataset_list)
    concat_list_size = len(concat_list)

    # Determine size of each list based on percentages
    val_len = int(np.floor(concat_list_size * valid_percent))
    test_len = int(np.floor(concat_list_size * test_percent))
    train_len = concat_list_size - (val_len + test_len)
    index_list = [train_len, val_len, test_len]

    # Randomize list and then split into train, validation, test
    train_dataset, val_dataset, test_dataset = torch.utils.data.random_split(concat_list, index_list)

    # Create the three data loaders
    if len(train_dataset) > 0:
        train_dataset = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size,  shuffle=True,
                                                    num_workers=num_workers, pin_memory=pin_memory)
    if len(val_dataset) > 0:
        val_dataset = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=True,
                                                  num_workers=num_workers, pin_memory=pin_memory)
    if len(test_dataset) > 0:
        test_dataset = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True,
                                                   num_workers=num_workers, pin_memory=pin_memory)

    return [train_dataset, val_dataset, test_dataset]
