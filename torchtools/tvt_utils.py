
import os
import collections
import torch
import torch.nn as nn
from tqdm import tqdm

import torch.nn.functional as F
from torchtools.cnnstats import CNNStats
from torchsummary import summary


# Train for one epoch and return entire loss/accuracy history
def train_epoch_history(model, optimizer, train_loader, device, scheduler=None):
    loss_history = []
    accuracy_history = []
    history_batch_size = 5
    running_loss = 0.0
    running_accuracy = 0.0

    model.train()
    total_loss = 0
    correct = 0
    total = 0
    pbar = tqdm(total=len(train_loader))

    for batch_idx, (data, target) in enumerate(train_loader):

        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        outputs = model(data)
        loss = nn.CrossEntropyLoss()(outputs, target)
        # loss = F.log_softmax(outputs, target)
        loss.backward()
        optimizer.step()
        total_loss += loss.item()
        total += target.size(0)
        _, predicted = torch.max(outputs.data, 1)
        batch_correct = (predicted == target).sum().item()
        correct += batch_correct
        pbar.update(1)

        # print statistics
        running_loss += loss.item()
        running_accuracy += batch_correct
        if batch_idx % history_batch_size == (history_batch_size-1):  # Accumulate history every N mini-batches
            loss_history.append(running_loss / (history_batch_size*target.size(0)))
            accuracy_history.append(running_accuracy / (history_batch_size*target.size(0)))
            running_accuracy = 0.0
            running_loss = 0.0

    if scheduler is not None:
        scheduler.step()

    pbar.close()

    return accuracy_history, loss_history


def train_epoch(model, optimizer, train_loader, device, scheduler=None):
    # Make sure model is in training mode
    model.train()
    total_loss = 0
    correct = 0
    total = 0
    pbar = tqdm(total=len(train_loader))

    for batch_idx, (data, target) in enumerate(train_loader):
        # Put data on device
        data, target = data.to(device), target.to(device)
        # Zero out optimizer gradients
        optimizer.zero_grad()
        # Put data through model
        outputs = model(data)
        # Calculate loss
        loss = nn.CrossEntropyLoss()(outputs, target)
        # Backpropagate loss through the net
        loss.backward()
        # Update/step optimizer
        optimizer.step()
        # Update metrics tracking loss/accuracy
        total_loss += loss.item()
        total += target.size(0)
        _, predicted = torch.max(outputs.data, 1)
        batch_correct = (predicted == target).sum().item()
        correct += batch_correct
        pbar.update(1)

    if scheduler is not None:
        scheduler.step()

    pbar.close()

    return correct/total, total_loss/total


def test(model, data_loader, device=torch.device("cpu"), show_cm=False, label_names=None):
    # Initialize confusion matrix and progress bar
    cm = CNNStats(class_names=label_names)
    pbar = tqdm(total=len(data_loader))

    # Make sure model is in evaluation mode
    model.eval()
    total_loss = 0
    correct = 0
    total = 0
    with torch.no_grad():
        # Run dat through the CNN
        for batch_idx, (data, target) in enumerate(data_loader):
            data, target = data.to(device), target.to(device)
            outputs = model(data)
            total_loss += F.cross_entropy(outputs, target).item()
            _, predicted = torch.max(outputs.data, 1)
            total += target.size(0)
            correct += (predicted == target).sum().item()
            if show_cm is True:
                cm.add_truth_pred(truth=target.cpu().numpy(), pred=predicted.cpu().numpy())

            pbar.update(1)
    pbar.close()

    if show_cm is True:
        # cm.print_confusion_matrix_latex()
        cm.print_confusion_matrix()
        cm.cm_plot()
    return correct / total, total_loss / total


def init_model_device(model, gpus_selected=[0]):
    # gpus_selected - list of GPUs I want to use.  If set to None or empty list then default to CPU
    # print("gpus_selected: ", gpus_selected)
    pin_memory = True
    if len(gpus_selected) == 1:
        gpu_device = "cuda:%d" % gpus_selected[0]
        device = torch.device(gpu_device if torch.cuda.is_available() else "cpu")
        print("Selected device: ", device, torch.cuda.is_available())
        model.to(device)
    elif len(gpus_selected) > 1 and torch.cuda.device_count() > 1:
        print("Use multiple GPUs to train!")
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = str(gpus_selected)
        gpu_device = "cuda:%d" % gpus_selected[0]
        device = torch.device(gpu_device if torch.cuda.is_available() else "cpu")
        # Use CUDA_VISIBLE_DEVICES to select which GPUs I want accessible
        model = nn.DataParallel(model, device_ids=gpus_selected)
        model.to(device)
        pass
    else:
        device = torch.device("cpu")
        pin_memory = False
    return model, device, pin_memory


def print_model_summary(model, data_shape):
    print("\nArchitecture parameter info:")
    print(model)
    print("\nArchitecture feature map & trainable parameter info:")
    summary(model, data_shape)
    print("\n")
    pass
