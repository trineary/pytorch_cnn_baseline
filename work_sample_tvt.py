

# options.py provides default list of hyperparameters/setting that can be set for the current run
# project_templates.py (referenced in options.py) holds all project specific settings that deviate from options
#       go to project_templates to make changes for this effort.
from options import args
from cnn_tvt import TVT


def train_test():

    # Initialize Train/Validation/Test class
    cnn_tvt = TVT(args)

    # Train the CNN
    cnn_tvt.train_cnn()

    # Test data through CNN
    cnn_tvt.test_cnn()

    print("-" * 50)
    print("Finished training CNN")
    print("-" * 50)
    pass


if __name__ == "__main__":
    train_test()
    pass

